#include "file_reader.h"

bool FileReader::open(const string &fileName)
{
    in.open(fileName);
    return in.is_open();
}

void FileReader::close(void)
{
    in.close();
}

string FileReader::getNextLine(void)
{
    string result;
    if(in.is_open()) getline(in, result);
    return result;
}

FileReader::~FileReader()
{
    close();
}
