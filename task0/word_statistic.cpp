#include "word_statistic.h"

#include <algorithm>

WordStatistic::WordStatistic(bool _ignoreCase)
{
    ignoreCase = _ignoreCase;
    wordsCount = 0; 
}

void WordStatistic::toLower(string &word) const
{
    transform(word.begin(), word.end(), word.begin(), [](unsigned char c){ return tolower(c); });
}

void WordStatistic::addWord(string word)
{
    if(ignoreCase) toLower(word);
    statistic[word]++;
    wordsCount++;
}

size_t WordStatistic::getWordFrequency(string word) const
{
    if(ignoreCase) toLower(word);
    umap::const_iterator i = statistic.find(word);
    if(i == statistic.end()) return 0;
    return i->second;
}

double WordStatistic::getWordFrequencyInPercents(string word) const
{
    if(ignoreCase) toLower(word);
    umap::const_iterator i = statistic.find(word);
    if(i == statistic.end()) return 0.0;
    return static_cast<double>(i->second) / wordsCount * 100.0;
}

void WordStatistic::reset(bool _ignoreCase)
{
    ignoreCase = _ignoreCase;
    wordsCount = 0;
    statistic.clear();
}

vector<StatisticItem> WordStatistic::getSortedStatistic(comparator comp) const
{
    vector<StatisticItem> items;
    for(const auto &item : statistic)
    {
        items.push_back(item);
    }
    sort(items.begin(), items.end(), comp);
    return items;
}
