#include <iostream>

#include "csv_file_writer.h"
#include "file_reader.h"
#include "string_parser.h"
#include "word_statistic.h"

using namespace std;

const int ARGS_COUNT = 3;
const int COLUMNS_COUNT = 3;
const char SEPARATOR = ',';

bool Comparator(const StatisticItem &a, const StatisticItem &b)
{
    return b.second < a.second; 
}

void FillWordStatistic(FileReader &in, WordStatistic &statistic)
{
    StringParser parser;
    while (!in.eof())
    {
        string current = in.getNextLine();
        vector<string> words = parser.splitIntoWords(current);
        for(const string &word : words)
        {
            statistic.addWord(word);
        }
    }
}

void PrintWordStatistic(CSVFileWriter &out, WordStatistic &statistic)
{
    vector<StatisticItem> sortedStatistic = statistic.getSortedStatistic(Comparator);
    for(StatisticItem stat : sortedStatistic)
    {
        out << stat.first << stat.second << statistic.getWordFrequencyInPercents(stat.first);
    }
}

int main(int argc, char** argv)
{
    if(argc != ARGS_COUNT)
    {
        cerr << "Wrong args count" << endl;
        return 1;
    }
    FileReader in;
    if(!in.open(argv[1]))
    {
        cerr << "Couldn't open file" << endl;
        return 1;
    }
    WordStatistic statistic(true);
    FillWordStatistic(in, statistic);
    in.close();
    CSVFileWriter out(SEPARATOR, COLUMNS_COUNT);
    out.open(argv[2]);
    PrintWordStatistic(out, statistic);
    out.close();
    return 0;
}
