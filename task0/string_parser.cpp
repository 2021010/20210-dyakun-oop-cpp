#include "string_parser.h"

vector<string> StringParser::splitIntoWords(const string &str)
{
    string word;
    vector<string> result;
    size_t currentWordPos = 0, strLen = str.length();
    for(size_t i = 0; i < strLen; i++)
    {
        if(isdigit(str[i]) || isalpha(str[i])) continue;
        if(currentWordPos == i) 
        {
            currentWordPos = i + 1;
            continue;
        }
        word.append(str, currentWordPos, i - currentWordPos);
        result.push_back(word);
        word.clear();
        currentWordPos = i + 1;
    }
    if(currentWordPos != strLen)
    {
        word.append(str, currentWordPos, strLen - currentWordPos);
        result.push_back(word);
    }
    return result;
}
