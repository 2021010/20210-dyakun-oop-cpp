#include "csv_file_writer.h"

CSVFileWriter::CSVFileWriter(char sep, size_t nColumns)
{
    separator = sep;
    columnsCount = nColumns;
    currentColumn = 0;
}

CSVFileWriter::~CSVFileWriter()
{
    close();
}

void CSVFileWriter::open(const string &fileName)
{
    out.open(fileName, ios::out | ios::app);
}

void CSVFileWriter::close(void)
{
    out.close();
}

void CSVFileWriter::writeSeparator(void)
{
    currentColumn++;
    if(currentColumn == columnsCount)
    {
        out << endl;
        currentColumn = 0;
    }
    else out << separator;
}

void CSVFileWriter::duplicateSeparator(string &str)
{
    for(size_t i = 0; i < str.length(); i++)
    {
        if(str[i] == separator) 
        {
            str.insert(i + 1, 1, separator);
            i++;
        }
    }
}

CSVFileWriter& operator<<(CSVFileWriter &out, const string &arg)
{
    string str = arg;
    out.duplicateSeparator(str); 
    out.out << str;
    out.writeSeparator();
    return out;
}

CSVFileWriter& operator<<(CSVFileWriter &out, const size_t &arg)
{
    out.out << arg;
    out.writeSeparator();
    return out;
}

CSVFileWriter& operator<<(CSVFileWriter &out, const double &arg)
{
    out.out << arg;
    out.writeSeparator();
    return out;
}
