#ifndef _FILE_READER_H_
#define _FILE_READER_H_

#include <fstream>
#include <string>

using namespace std;

class FileReader
{
private:
    ifstream in;
    FileReader(const FileReader &f){ (void)f; }

public:
    FileReader(void){}
    ~FileReader();
    bool open(const string &fileName);
    void close(void);
    string getNextLine(void);
    bool eof(void) const { return in.eof(); }
};

#endif
