#ifndef _WORD_STATISTIC_H_
#define _WORD_STATISTIC_H_

#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

typedef unordered_map<string, size_t> umap;
typedef struct pair<string, size_t> StatisticItem;
typedef bool (*comparator)(const StatisticItem&, const StatisticItem&);

class WordStatistic
{
private:
    umap statistic;
    size_t wordsCount;
    bool ignoreCase;
    WordStatistic(const WordStatistic &w){ (void)w; }
    void toLower(string &word) const;

public:
    WordStatistic(bool _ignoreCase);
    ~WordStatistic(){}
    void addWord(string word);
    size_t getWordFrequency(string word) const;
    double getWordFrequencyInPercents(string word) const;
    void reset(bool _ignoreCase);
    vector<StatisticItem> getSortedStatistic(comparator comp) const;
};

#endif
