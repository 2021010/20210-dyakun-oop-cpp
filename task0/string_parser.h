#ifndef _STRING_PARSER_H_
#define _STRING_PARSER_H_

#include <string>
#include <vector>

using namespace std;

class StringParser
{
public:
    StringParser(){}
    StringParser(const StringParser &s){ (void)s; }
    ~StringParser(){}
    vector<string> splitIntoWords(const string &str);
};

#endif
