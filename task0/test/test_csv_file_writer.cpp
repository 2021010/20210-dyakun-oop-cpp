#include "csv_file_writer.h"

#include <fstream>
#include <gtest/gtest.h>
#include <string>

using namespace std;

const char fileName[] = "test.txt";

TEST(_CSVFileWriter, Operators)
{
    CSVFileWriter out(',', 2);
    ifstream in;
    string str;
    out.open(fileName);

    out << "two" << 2UL;
    out << "one" << 1UL;

    out.close();
    in.open(fileName);
    getline(in, str);
    EXPECT_STREQ(str.c_str(), "two,2");
    getline(in, str);
    EXPECT_STREQ(str.c_str(), "one,1");
    in.close();
    remove(fileName);
}

TEST(_CSVFileWriter, Separator_in_args)
{
    CSVFileWriter out(',', 2);
    ifstream in;
    string str;
    out.open(fileName);

    out << "two, three" << 2UL;
    out << "one" << 1UL;

    out.close();
    in.open(fileName);
    getline(in, str);
    EXPECT_STREQ(str.c_str(), "two,, three,2");
    getline(in, str);
    EXPECT_STREQ(str.c_str(), "one,1");
    in.close();
    remove(fileName);
}
