#include "file_reader.h"

#include <fstream>
#include <gtest/gtest.h>
#include <string>

using namespace std;

const char fileName[] = "test.txt";

TEST(_FileReader, open)
{
    FileReader in;
    ofstream newFile;
    newFile.open(fileName, ios::out | ios::app); // create new file
    newFile.close();

    int ret = in.open(fileName);

    EXPECT_NE(ret, 0);
    in.close();
    remove(fileName);
}

TEST(_FileReader, getNextLine)
{
    ofstream newFile;
    newFile.open(fileName, ios::out | ios::app); // create new file
    newFile << 1 << endl;
    newFile << 2;
    newFile.close();
    FileReader in;
    in.open(fileName);
    size_t stringCount = 0;
    string str;

    while(!in.eof())
    {
        str = in.getNextLine();
        stringCount++;
    }

    EXPECT_EQ(stringCount, 2);
    EXPECT_STREQ(str.c_str(), "2");
    in.close();
    remove(fileName);
}
