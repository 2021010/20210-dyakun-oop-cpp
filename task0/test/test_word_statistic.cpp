#include "word_statistic.h"

#include <gtest/gtest.h>
#include <string>
#include <vector>

using namespace std;

TEST(_WordStatistic, addWord)
{
    WordStatistic ws(false);
    string one = "one";
    string two = "two";
    string Two = "Two";

    ws.addWord(one);
    ws.addWord(two);
    ws.addWord(two);
    ws.addWord(Two);
    ws.addWord(Two);

    EXPECT_EQ(ws.getWordFrequency(one), 1);
    EXPECT_EQ(ws.getWordFrequency(two), 2);
    EXPECT_EQ(ws.getWordFrequency(Two), 2);
}

TEST(_WordStatistic, addWord_ignore_case)
{
    WordStatistic ws(true);
    string one = "one";
    string two = "two";
    string Two = "Two";

    ws.addWord(one);
    ws.addWord(two);
    ws.addWord(two);
    ws.addWord(Two);
    ws.addWord(Two);

    EXPECT_EQ(ws.getWordFrequency(one), 1);
    EXPECT_EQ(ws.getWordFrequency(two), 4);
    EXPECT_EQ(ws.getWordFrequency(Two), 4);
}

bool Comparator(const StatisticItem &a, const StatisticItem &b)
{
    return b.second < a.second;
}

TEST(_WordStatistic, getSortedStatistic)
{
    WordStatistic ws(false);
    string one = "one";
    string two = "two";
    ws.addWord(one);
    ws.addWord(two);
    ws.addWord(two);
    vector<StatisticItem> expected = { {two, 2}, {one, 1}};

    auto result = ws.getSortedStatistic(Comparator);

    EXPECT_EQ(result[0], expected[0]);
    EXPECT_EQ(result[1], expected[1]);
}
