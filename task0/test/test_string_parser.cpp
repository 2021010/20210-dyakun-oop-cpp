#include "string_parser.h"

#include <gtest/gtest.h>
#include <string>
#include <vector>

using namespace std;

TEST(_StringParser, splitIntoWords)
{
    string input = "one two three\n";
    vector<string> expected = {"one", "two", "three"};
    StringParser parser;

    vector<string> result = parser.splitIntoWords(input);

    EXPECT_EQ(result[0], expected[0]);
    EXPECT_EQ(result[1], expected[1]);
    EXPECT_EQ(result[2], expected[2]);
}
