#ifndef _CSV_FILE_WRITER_H_
#define _CSV_FILE_WRITER_H_

#include <fstream>

using namespace std;

class CSVFileWriter
{
private:
    ofstream out;
    char separator;
    size_t columnsCount;
    size_t currentColumn;
    CSVFileWriter(const CSVFileWriter &w){ (void)w; }
    void writeSeparator(void);
    void duplicateSeparator(string &str);

public:
    CSVFileWriter(char separator, size_t columnsCount);
    ~CSVFileWriter();
    void open(const string &fileName);
    void close(void);
    friend CSVFileWriter& operator<<(CSVFileWriter &out, const string &arg);
    friend CSVFileWriter& operator<<(CSVFileWriter &out, const size_t &arg);
    friend CSVFileWriter& operator<<(CSVFileWriter &out, const double &arg);
};

#endif
